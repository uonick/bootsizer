bootsizer
=========

Bootstrap Chrome Resizer plugin

![Screenshot] (http://uonick.ru/bootsizer/browser.png)

[Webpage] (http://uonick.ru/bootsizer/)

[Chrome Store] (https://chrome.google.com/webstore/detail/bootsizer/fnlkljfogjamjaemldpapdokokkifmib)
